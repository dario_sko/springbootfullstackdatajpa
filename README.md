- [Was ist Spring Data JPA](#was-ist-spring-data-jpa)
- [Aufbau (UML)](#aufbau-uml)
  - [Spring Initializr](#spring-initializr)
- [Verbindung Datenbank](#verbindung-datenbank)
- [Mapping Entities in die Datenbank](#mapping-entities-in-die-datenbank)
- [Verschiedene JPA Annotation](#verschiedene-jpa-annotation)
- [Repository](#repository)
  - [Testen von Repo](#testen-von-repo)
- [Embeddable und Embedded](#embeddable-und-embedded)
- [JPA Query Methods](#jpa-query-methods)
- [Custom Query Methods](#custom-query-methods)
- [Modifying](#modifying)
- [Relationship](#relationship)
  - [Cascading Types](#cascading-types)
  - [Fetch Types](#fetch-types)
  - [Uni & Bi Directional Relationship](#uni--bi-directional-relationship)
  - [OneToOne](#onetoone)
  - [OneToMany](#onetomany)
  - [ManyToOne](#manytoone)
  - [ManyToMany](#manytomany)
- [Paging und Sorting](#paging-und-sorting)

# Was ist Spring Data JPA

Ist eine Schnittstelle um JPA-Implementierungen (ORM Frameworks) leichter in Spring Boot Anwendungen einbinden und verwenden zu können. Wegen der Schnittestelle kann diese implementierte Technologie leichter durch andere ausgetauscht werden.

# Aufbau (UML)

![Untitled](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/84924f95-4190-4039-9537-f8ce86b4dc55/Untitled.png)

## Spring Initializr

**Dependencies:**

- Spring Data JPA
- Lombok
- MySQL Driver
- Spring Web

# Verbindung Datenbank

application.properties

```java
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://192.168.227.21:3306/schooldb
spring.datasource.username=root
spring.datasource.password=123
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.show-sql: true
```

# Mapping Entities in die Datenbank

Java-Klasse muss die Annotation @Entity hinzugefügt werden. Definiert, dass eine Klasse einer Tabelle zugeordnet werden kann.

**Lombok** Annotationen um alles im Hintergrund generieren zu lassen:

@Data erstellt alle Getter und Setter

@NoArgsConstructor - Default Konstruktor

@AllArgsConstructor - Alle Parameter Kontstruktor

@Builder - Builder wird für die Klasse generiert

Beim Starten der Application werden diese Entity Klasse als Tabellen mit allen Attributen in der Datenbank erstellt. Es wird die Java Namenskonvetion in die SQL Konvention umgewandelt. ( firstName → first_name)

# Verschiedene JPA Annotation

@Table(name = "tabellenName") → eigene Name der Tabelle angeben

```java
@Table(name = "tbl_student")
public class Student {
```

@Column(name = "spaltenName") → eigene Name der Spalte angeben  

```java
@Column(name = "email_address")
    private String emailId;
```

@Id → Primary ID

@SequenceGenerator - erstellen ein Inkrementierungsmethode

@GeneratedValue - Verwendung der erstellen Methode

Am besten ist immer eine eigene Methode zu erstellen und nicht eine Standardmethode zu verwenden, da sie bei jeder Technologie funktioniert.

```java
@Id
@SequenceGenerator(
            name = "student_sequence",
            sequenceName = "student_sequence";
            allocationSize = 1
)
@GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "student_sequence"
)
private Long studentId;
```

**Contstraints:**

Um Constraints zu erstellen müssen der Tabelle die Annotation UniqueConstrains hinzugefügt werden

```java
@Table(
        name = "tbl_student",
        uniqueConstraints = @UniqueConstraint(
                name = "name_von_constraint",
                columnNames = "vorhandener_splatenName zB email_address"
        )
)
```

Wenn eine Attribute nicht null sein darf muss der Parameter nullable gesetzt werden

```java
@Column(name = "email_address",
        nullable = false
    )
    private String emailId;
```

# Repository

Interface JpaRepository enthält alle wichtigen CRUD Methoden um mit der Datenbank interagieren zu können.

## Testen von Repo

@DataJpaTest - wird für das Testen eines Repos verwendet. Danach werden die Daten von der Datenbank wieder gelöscht

In unserem Fall verwenden wir aber den normalen Test (@SpringBootTest) und werden daher Daten auf der Datenbank erhalten. [StudentRepositoryTest](./src\test\java\com\springboot\SpringBootJPA\repository\StudentRepositoryTest.java)

# Embeddable und Embedded

Wird verwendet wenn man keine seperate Tabelle erstellen möchte aber die Informationen in eine seperate Klasse abstrahieren möchte.

@Embeddable wird in der Klasse benützt die einbettbar werden soll

@Embedded wird in der Klasse benützt wo die Klasse verwendet wird

Da es so viele Konflikten mit den Namen der Eigenschaften geben kann zB firstname - firstname_embedded gibt es die Möglichkeit die Namen zu überschreiben.

```java
@AttributeOverrides({
        @AttributeOverride(
				// Name der Variable
                name = "name",
				// Name in der Datenbankspalte
                column = @Column(name = "guardian_name")
        ),
        @AttributeOverride(
                name = "email",
                column = @Column(name = "guardian_email")
        ),
        @AttributeOverride(
                name = "mobile",
                column = @Column(name = "guardian_mobile"))
})
```

# JPA Query Methods

Erstellung von Abfragen-Methoden mit JPA Keywords

[https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation)

# Custom Query Methods

@Query Annotation

?1 → Parameter Index

:parametername → Zugriff auf Parametername

```java
//JPQL - Parameter Index
    @Query("select s from Student s where s.emailId = ?1")
    Student getStudentByEmailAddress(String emailId);

    @Query("select s.firstName from Student s where s.emailId = ?1")
    String getStudentFirstNameByEmailAddress(String emailId);

//MySQL - Zugriff auf Parametername
//Native Query
    @Query(
            value = "SELECT * FROM tbl_student s WHERE s.email_address = :emailId",
            nativeQuery = true
    )
    Student getStudentByEmailAddressNative(String emailId);
```

# Modifying

Um eine Modifying Query zu erstellen muss die Annotation @Modifying verwendet werden.

@Transactional → wird verwendet um alle Queries als Transaktion zu behandeln, falls ein Fehler auftritt werden alle Änderungen rückgängig gemacht (Rollback).

Transaktionen in Java beziehen sich im Allgemeinen auf eine Reihe von Aktionen, die alle erfolgreich abgeschlossen werden müssen.

```java
@Modifying
@Transactional
@Query(
value = "update tbl_student set first_name = ?1 where email_address = ?2",
        nativeQuery = true
)
int updateStudentNameByEmailId(String firstName, String emailId);
```
# Relationship

## Cascading Types

Ein Fremdschlüssel mit Cascade.ALL bedeutet, dass zB beim Löschen eines Datensatzes in der übergeordneten Tabelle automatisch die entsprechenden Datensätze in der untergeordneten Tabelle gelöscht werden. Dies wird in SQL Server als kaskadierendes Löschen bezeichnet.

```java
@OneToOne
            (
                    cascade = CascadeType.ALL
            )
```

## Fetch Types

Es gibt zwei Arten:

- FetchType.LAZY → Es gibt nur die Entity zurück ohne Relations Entities - nur wenn gebraucht
- FetchType.EAGER → Es gibt Entity mit allen Relation Entities zurück

## Uni & Bi Directional Relationship

Mit mappedBy referenziert man auf das im anderen schon erstellem OneToOne Mapping

```java
public class Course {
@OneToOne(
            mappedBy = "course"
    )
    private CourseMaterial courseMaterial;
}
```

## OneToOne

CourseMaterial kann nicht ohne Course bestehen.

OneToOne Annotation muss verwendet werden sowie JoinColumn um den ForeignKey anzugeben

Um zu verhindern das es ein CourseMaterial ohne Course gibt muss der Parameter von optional auf false gesetzt werden

```java
@OneToOne
            (
                    cascade = CascadeType.ALL,
                    fetch = FetchType.LAZY,
                    optional = false
            )
    @JoinColumn(
            name = "course_id",
            referencedColumnName = "courseId"
    )
    private Course course;
```

## OneToMany

Die Teacher Klasse kann eine Liste von Kurse beinhalten

```java
@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(
            name = "teacher_id",
            referencedColumnName = "teacherId"
    )
    private List<Course> courses;
```

## ManyToOne

```java
@ManyToOne(
            cascade = CascadeType.ALL
    )
    @JoinColumn(
            name = "teacher_id",
            referencedColumnName = "teacherId"
    )
    private Teacher teacher;
```

## ManyToMany

Mit @JoinTable wird eine Tabelle erstellt die beide ForeignKeys beinhaltet und so auf beide Entities referenziert

```java
@ManyToMany(
            cascade = CascadeType.ALL
    )
    @JoinTable(
            name = "student_course_map",
            joinColumns = @JoinColumn(
                    name = "course_id",
                    referencedColumnName = "courseId"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "student_id",
                    referencedColumnName = "studentId"
            )
    )
    private List<Student> students;

    public void addStudents(Student student) {
        if (students == null) students = new ArrayList<>();
        students.add(student);
    }
```

# Paging und Sorting

Die Paginierung ist oft hilfreich, wenn wir ein großes Dataset haben und es dem Benutzer in kleineren Blöcken präsentieren möchten.

Außerdem müssen wir diese Daten beim Paging oft nach einigen Kriterien sortieren.