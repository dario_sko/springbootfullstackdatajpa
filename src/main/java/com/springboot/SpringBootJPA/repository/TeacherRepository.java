package com.springboot.SpringBootJPA.repository;

import com.springboot.SpringBootJPA.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}